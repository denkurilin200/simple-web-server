const { EventEmitter } = require('events');
const { IncomingMessage, ServerResponse } = require('http');

/**
 * This callback is displayed as a global member.
 * @callback reqHandler
 * @param {IncomingMessage} req
 * @param {ServerResponse} res
 */

class Router extends EventEmitter {
	/**
	 *
	 * @param {String} url
	 * @param {reqHandler} callback
	 */
	get(url, callback) {
        console.log(`GET:${url}`)
		this.on(`GET:${url}`, callback);
    }

    /**
     * 
     * @param {String} url 
     * @param {reqHandler} callback 
     */
    post(url, callback) {
        this.on(`POST:${url}`, callback)
    }
}

const router = new Router();

module.exports = router;
