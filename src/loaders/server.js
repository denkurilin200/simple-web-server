const http = require('http');
const router = require('../eventEmitter/router');
const glob = require('glob');
const path = require('path');

const routePaths = glob.sync(path.resolve(__dirname, '../api/**/*.js'));
routePaths.forEach((routePath) => {
	require(routePath);
});

const server = http.createServer((req, res) => {
	if (req.url.includes('static')) {
		router.emit(`${req.method}:/static`, req, res);
	}
	router.emit(`${req.method}:${req.url}`, req, res);
});

server.listen(process.env.PORT || 5000, () => console.log(`Server listening on ${process.env.PORT || 5000}`));
