const router = require('../../eventEmitter/router');
const fs = require('fs');
const path = require('path');

router.get('/static', (req, res) => {
	try {
		const filePath = req.url;
		console.log(filePath);

		const data = fs.readFileSync(path.resolve(__dirname, '../..' + filePath));

		res.statusCode = 200;
		res.write(data);
		res.end();
	} catch (err) {
		console.error(err);
		res.statusCode = 404;
		res.write('No such file');
		res.end();
	}
});
